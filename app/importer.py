import pandas as pd
from app.Illuminate.ParamStorage import ParamStorage


def is_valid_len(u, v, w, db):
    if len(u) == len(v) and len(u) == len(w) and len(u) == len(db):
        return True

    return False


def is_invalid_number(invalid: str, params: list):
    for item in params:
        if item == invalid:
            return True

    return False


def get_key_round(key):
    if isinstance(key, float):
        return key

    if isinstance(key, str):
        return float(key.strip())

    return float(key)


def process(storage: ParamStorage) -> list:
    delete_number = '-32768'

    date_time = storage.get_param_by_name('date_time')
    expedition_number = storage.get_param_by_name('expedition_number')

    file_ref = storage.get_file_by_type('ref')
    file_data = storage.get_file_by_type('data')

    ref_frame = pd.read_csv(file_ref, sep=',,', engine="python",
                            names=["id", "latitude", "longitude", "distance", "speed", "maxDepth", "depth"])
    data_frame = pd.read_csv(file_data, sep=',,', engine="python",
                             names=['U', 'V', 'W', 'Db'])

    ref_count = ref_frame.count()[0]
    dat_count = data_frame.count()[0]

    if ref_count != dat_count:
        raise Exception(
            "Кол-во строк в файлах ref и data не совпадают (ref:{})(data:{})".format(ref_count, dat_count))

    result = []
    for i in range(ref_count):
        # Компонента потока U
        u = data_frame.at[i, 'U'].split(',')
        # Компонента потока V
        v = data_frame.at[i, 'V'].split(',')
        # Компонента потока W
        w = data_frame.at[i, 'W'].split(',')
        # Уровень сигнала
        db = data_frame.at[i, 'Db'].split(',')

        # Разбиение по глубине
        item = ref_frame.at[i, 'depth'].split(',')
        # Фильтр по скорости лодки
        speed = ref_frame.at[i, 'speed']

        if is_valid_len(u, v, w, db) is False:
            # print("Skip row at line {} [{} {} {} {} ]".format(
            #     i, len(u), len(v), len(w), len(db)))
            continue

        if is_invalid_number(delete_number, [str(int(speed))]):
            continue

        length = len(item)
        depths = set()
        depth_values = {}
        for j in range(length):

            if is_invalid_number(delete_number, [u[j], v[j], w[j], db[j]]):
                continue

            key = get_key_round(item[j])
            depth_values[key] = {'u': u[j], 'v': v[j], 'w': w[j], 'db': db[j]}
            depths.add(int(key))

        result.append(dict({
            'date': date_time,
            'step_id': int(ref_frame.at[i, 'id']),
            'latitude': ref_frame.at[i, 'latitude'],
            'longitude': ref_frame.at[i, 'longitude'],
            'distance': ref_frame.at[i, 'distance'],
            'speed': ref_frame.at[i, 'speed'],
            'max_depth': ref_frame.at[i, 'maxDepth'],
            'list_depths': list(depths),
            'depths': depth_values,
            'expedition_number': expedition_number
        }))

    return result
